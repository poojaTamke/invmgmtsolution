import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/data.service';
import { MatPaginator } from '@angular/material';

@Component({
  selector: 'app-procurement-categories',
  templateUrl: './procurement-categories.component.html',
  styleUrls: ['./procurement-categories.component.scss']
})
export class ProcurementCategoriesComponent implements OnInit {
  procurementCategories$:object;
  suppliers$:object
  procurementCategoriesArr:any[]=[];
  suppliersArr:any[]=[];
  suppliersDataLoaded: Promise<boolean>;
  displayedColumns : String[]=['supplier','name','slug','url'];
  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public dataSource: any;  
  paginator: MatPaginator;
  constructor(private data:DataService) { }

  ngOnInit() {

    this.getProcurementCategoriesList();
  }
  getProcurementCategoriesList(){
    this.data.getSuppliers().subscribe(suppliersData=>
      {
         this.suppliers$ = suppliersData;
         for(var supplier in this.suppliers$){
           this.suppliersArr[supplier]=this.suppliers$[supplier];
         }        
         this.data.getProcurementCategories().subscribe( data=>
            {
             this.procurementCategories$=data;
             for(var procurementCategory in this.procurementCategories$){
               this.procurementCategoriesArr[procurementCategory]=this.procurementCategories$[procurementCategory];
             }
             for(let i =0;i<this.procurementCategoriesArr.length;i++){
              
               let supplierName = this.suppliersArr.find(x=>x.id == this.procurementCategoriesArr[i].supplier).name;
               this.procurementCategoriesArr.concat('supplierName');
               this.procurementCategoriesArr[i].supplierName = supplierName;
               this.suppliersDataLoaded = Promise.resolve(true);
             }      
             this.totalSize = this.procurementCategoriesArr.length;
             this.iterator();                
           });
 
       
         
     });

  }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.procurementCategoriesArr.slice(start, end);
    this.dataSource = part;
  }

 


}
