import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcurementCategoriesComponent } from './procurement-categories.component';

describe('ProcurementCategoriesComponent', () => {
  let component: ProcurementCategoriesComponent;
  let fixture: ComponentFixture<ProcurementCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcurementCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcurementCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
