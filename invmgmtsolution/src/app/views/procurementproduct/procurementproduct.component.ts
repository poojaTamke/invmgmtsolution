import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/data.service';
import { MatPaginator } from '@angular/material';

@Component({
  selector: 'app-procurementproduct',
  templateUrl: './procurementproduct.component.html',
  styleUrls: ['./procurementproduct.component.scss']
})
export class ProcurementproductComponent implements OnInit {

  procurementProduct$:object;
  procurementProductsArr :any[]=[];
  displayedColumns:string[]=['sku','name'];
  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public dataSource: any;  
  paginator: MatPaginator;
  constructor(private data:DataService) { }

  ngOnInit() {

     this.getProcurementProducts();
  }
  getProcurementProducts(){
    this.data.getProcurementProducts().subscribe(data => 
      {
        this.procurementProduct$=data;
        for(let i in this.procurementProduct$){
          this.procurementProductsArr[i]=this.procurementProduct$[i];
        }
        this.totalSize = this.procurementProductsArr.length;
        this.iterator();
      }

    );
  }
  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.procurementProductsArr.slice(start, end);
    this.dataSource = part;
  }
}
