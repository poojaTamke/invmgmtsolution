import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcurementproductComponent } from './procurementproduct.component';

describe('ProcurementproductComponent', () => {
  let component: ProcurementproductComponent;
  let fixture: ComponentFixture<ProcurementproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcurementproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcurementproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
