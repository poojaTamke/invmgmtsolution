import { Component, OnInit } from '@angular/core';
import {DataService} from '../../data.service';
import { Observable } from 'rxjs';
import { MatPaginator, MatTableDataSource } from '@angular/material';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  displayedColumns: string[] = ['sku', 'name'];
  products$:Object;
  productsArr:any[]=[];
  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public dataSource: any;  
  paginator: MatPaginator;
  accessToken:string;
  constructor(private data:DataService) { }

  ngOnInit() {
    this.accessToken = localStorage.getItem('accessToken');
    this.getProductList();
     
     
  }
  
  getProductList():void{
    this.data.getProductList().subscribe(data => 
      {
        this.products$ = data
        for(let i in this.products$){
          this.productsArr[i] = this.products$[i];
        }        
        this.totalSize = this.productsArr.length;
        this.iterator();
      } 
 
     
     );
  }
  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.productsArr.slice(start, end);
    this.dataSource = part;
  }
}
