import { Component, OnInit } from '@angular/core';
import {DataService} from '../../data.service';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { TryCatchStmt } from '@angular/compiler';
import { Route, Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;
  result:string;
  accessToken:string;
  error:string;
  loginResult$: Object;
  show: boolean = false;
  userLogInfo = {     username: '',     password: ''   };
  constructor(private data: DataService,private router:Router) { }

  ngOnInit() {
  }

  login() : void {
    
    this.userLogInfo.username = this.username;
    this.userLogInfo.password = this.password;
    
    //var userLogin = JSON.stringify(this.userLogInfo);
   /* this.data.login(this.userLogInfo).subscribe(
     data => this.loginResult$ = data 

    
    );*/
    this.data.login(this.userLogInfo).subscribe(data=>
      {
        this.loginResult$=data;         
        this.show = true;
        this.accessToken = this.loginResult$['token'];
        localStorage.setItem('accessToken', this.accessToken);
        console.log(localStorage.getItem('accessToken'));       
        this.router.navigate(["products"]);
      }
    
    );
   
    
  }
  
}
