import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { DataService } from 'app/data.service';
import { MatPaginator } from '@angular/material';

@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.component.html',
  styleUrls: ['./productdetails.component.scss']
})
export class ProductdetailsComponent implements OnInit {

  product$:object;
  inventories$:object;
  productDetails$:object=[];//{id: 95, default_location: false, quantity: 100, quantity_needed_labels: 200, comments: "comment2",location: 85};
  inventoriesArr:any[];
  locations$:object;
  locationsArr:any[]=[];
  productDetailsArr:any[]=[];//[{id: 95, default_location: false, quantity: 100, quantity_needed_labels: 200, comments: "comment2",location: 85},{id: 95, default_location: false, quantity: 100, quantity_needed_labels: 200, comments: "comment2",location: 85},{id: 95, default_location: false, quantity: 100, quantity_needed_labels: 200, comments: "comment2",location: 85}];
  displayedColumns:string[]=['location','quantity','quantity_needed_labels','comments','delete'];
  productName:string;
  qty:number;
  productDetailsLoaded: Promise<boolean>;
  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public dataSource: any;  
  paginator: MatPaginator;
  constructor(private route:ActivatedRoute,private data:DataService) { 
    this.route.params.subscribe(params=>this.product$=params.id)
  }
  ngOnInit() {
    
    this.getProductDetails();
    
  }
  getProductDetails(){
    this.data.getLocations().subscribe(locationData=>
      {
        this.locations$ = locationData;
        for(let i in this.locations$){
          this.locationsArr[i]=this.locations$[i];
        }
        this.data.getProductDetails(this.product$).subscribe(data=>
          {
            this.product$=data;
            this.inventoriesArr = this.product$['inventories'];
            
            if(this.inventoriesArr.length>0){
              this.productName = this.product$['name'];
              for(let i =0;i<this.inventoriesArr.length;i++){
                this.data.getInventory(this.inventoriesArr[i]).subscribe(inventData=>
                  {
                    this.inventories$=inventData;
                    this.productDetailsArr.push(this.inventories$);

                    for(let i =0;i<this.productDetailsArr.length;i++){
              
                      let locationName = this.locationsArr.find(x=>x.id == this.productDetailsArr[i].location).abbreviation_slug;
                      this.productDetailsArr.concat('locationName');
                      this.productDetailsArr[i].locationName = locationName;
                      
                    }                             
                    this.totalSize = this.productDetailsArr.length;
                    this.iterator();
                    this.productDetailsLoaded = Promise.resolve(true);
                  });
                 
              }
            }
            
          });  

      });
  }
  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.productDetailsArr.slice(start, end);
    this.dataSource = part;
  }
  decreaseQty(product):void{
    this.qty = product.quantity;
    this.qty=this.qty-1;
    product.quantity = this.qty;
    //return product;
    
  }
  increaseQty(product):void{
    this.qty = product.quantity;
    this.qty=this.qty+1;
    product.quantity = this.qty;
    //return product;
    
  }

}
