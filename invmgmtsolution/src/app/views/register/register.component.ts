import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  email:string;
  username:string;
  password:string;
  displayMsg:string;
  registerResult$:object;
  successFlag:boolean=false;
  registerInfo = {email:'',username:'',password:''};
  constructor(private data:DataService) { }

  ngOnInit() {
  }

  register():void{

      this.registerInfo.email=this.email;
      this.registerInfo.username=this.username;
      this.registerInfo.password=this.password;

      this.data.register(this.registerInfo).subscribe(data=>{

          this.registerResult$=data;

          let token = this.registerInfo['token'];
          if(token){
            this.displayMsg="Registered successfully...";
            this.successFlag=true;
            this.email='';
            this.username='';
            this.password='';
          }
          else{
            this.displayMsg="Error occurred while registration...";
            this.successFlag=false;
          }

      });

  }

}
