import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';

import { CalendarModule,  } from 'angular-calendar';
import { SharedModule } from '../shared/shared.module';

import { FooterComponent } from '../main-layout/footer/footer.component';
import { ModalsComponent } from './modals/modals.component';
import { TypographyComponent } from './css/typography/typography.component';
import { IconsComponent } from './css/icons/icons.component';

import { GridComponent } from './css/grid/grid.component';
import { MediaObjectComponent } from './css/media-object/media-object.component';
import { UtilitiesComponent } from './css/utilities/utilities.component';
import { ImagesComponent } from './css/images/images.component';
import { ColorsComponent } from './css/colors/colors.component';
import { ShadowComponent } from './css/shadow/shadow.component';

import { HelpComponent } from './help/help.component';
import { LoginComponent } from './login/login.component';
import {MatButtonModule, MatCheckboxModule,MatFormFieldModule,MatInputModule,
  MatCardModule, MatDialogModule, MatTableModule,
  MatToolbarModule, MatMenuModule,MatIconModule, MatProgressSpinnerModule,MatPaginatorModule} from '@angular/material';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { ProductsComponent } from './products/products.component';
import { ProcurementCategoriesComponent } from './procurement-categories/procurement-categories.component';
import { ProcurementproductComponent } from './procurementproduct/procurementproduct.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule, 
    MatDialogModule, 
    MatTableModule,
    MatToolbarModule, 
    MatMenuModule,
    MatIconModule, 
    MatProgressSpinnerModule,
    ShowHidePasswordModule.forRoot(),
    HttpClientModule,
    MatPaginatorModule,
    
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: ''
    }),
    CalendarModule.forRoot()
  ],
  declarations: [
    FooterComponent,    
    ModalsComponent,
    TypographyComponent,
    IconsComponent,       
    GridComponent,
    MediaObjectComponent,
    UtilitiesComponent,
    ImagesComponent,
    ColorsComponent,
    ShadowComponent,    
    HelpComponent,
    LoginComponent,
    RegisterComponent,
    ProductsComponent,
    ProcurementCategoriesComponent,
    ProcurementproductComponent,
    ProductdetailsComponent,
    LogoutComponent,

  ],
  exports: [
    FooterComponent,    
    ModalsComponent,
    TypographyComponent,
    IconsComponent, 
    GridComponent,
    MediaObjectComponent,
    UtilitiesComponent,
    ImagesComponent,
    ColorsComponent,
    ShadowComponent,

  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ViewsModule { }
