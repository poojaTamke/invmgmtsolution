import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    this.logout();
  }
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('accessToken');
    this.router.navigate(["login"]);
}
}
