import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @ViewChild('sidenav') sidenav: ElementRef;

  clicked: boolean;  
  accessToken:string;
  constructor() {
    this.clicked = this.clicked === undefined ? false : true;
  }

  ngOnInit() {
    this.getAccessToken();
  }

  setClicked(val: boolean): void {
    this.clicked = val;
  }
  getAccessToken(){
    this.accessToken = localStorage.getItem('accessToken');
    return this.accessToken;
  }

}
