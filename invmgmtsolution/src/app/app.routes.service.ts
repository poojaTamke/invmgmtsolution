
import { ModalsComponent } from './views/modals/modals.component';
import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { NotFoundComponent } from './views/errors/not-found/not-found.component';
import {LoginComponent} from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import {ProductsComponent} from './views/products/products.component';
import { ProcurementCategoriesComponent } from './views/procurement-categories/procurement-categories.component';
import { ProcurementproductComponent } from './views/procurementproduct/procurementproduct.component';
import { ProductdetailsComponent } from './views/productdetails/productdetails.component';
import { LogoutComponent } from './views/logout/logout.component';
const routes: Route[] = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },  
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'logout',
    component:LogoutComponent
  },
  {
    path:'register',
    component:RegisterComponent
  },
  {
    path:'products',
    component:ProductsComponent
  },
  {
    path:'productdetails/:id',
    component:ProductdetailsComponent
  },
  {
    path:'procurementcategories',
    component:ProcurementCategoriesComponent
  },
  {
    path:'procurementproducts',
    component:ProcurementproductComponent
  },
  { path: 'modals', component: ModalsComponent},
  { path: '**', component: NotFoundComponent },

  
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);

