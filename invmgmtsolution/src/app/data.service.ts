import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  login(userLogInfo) {
    
    return this.http.post('http://138.68.106.87/api/auth/login/',userLogInfo)
  }
  register(registerInfo){
    return this.http.post('http://138.68.106.87/api/auth/register/',registerInfo);
  }
  getProductList() {
    return this.http.get('http://138.68.106.87/api/catalog/products/')
  }
  getProcurementCategories(){

    return this.http.get('http://138.68.106.87/api/procurement/categories/');
  }
  getProcurementProducts(){
    return this.http.get('http://138.68.106.87/api/procurement/products/');
  }
  getProductDetails(id){
    return this.http.get('http://138.68.106.87/api/catalog/products/'+id);
  }
  getInventory(id){
    return this.http.get('http://138.68.106.87/api/catalog/inventories/'+id);
  }
  getLocations(){
    return this.http.get('http://138.68.106.87/api/inventory/locations/');
  }
  getSuppliers(){
    return this.http.get('http://138.68.106.87/api/procurement/suppliers/');
  }
}
